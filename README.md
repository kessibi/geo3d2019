# Geo3D2019

The goal of this project was to implement a raytracer in C++. The OpenGL/QT part
was given and we had to implement sphere/cube/cylinder and menger sponges
intersections. Then we were basically free to add what we wanted to work on in
the program. I implemented Brandon Pelfrey's Fast-BVH to generate images faster
than with the test each node method.

Fast-BVH: https://github.com/brandonpelfrey/Fast-BVH

I added specular lighting, diffuse lighting, reflection, refraction and a way to
animate the whole process.
You'll find a folder with generated images in the repo.

`make && ./bin/projet_raytracing`
