#include <QGLViewer/simple_viewer.h>
#include <matrices.h>
#include <primitives.h>

#include "raytracing.h"

// Légende :
// AC : à compléter
// DC : déjà complet (ne pas toucher sauf en cas d'extrême urgence)
// TC : théoriquement complet (mais modifications possibles en fonction de votre
// projet)

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  SimpleViewer::init_gl();
  SimpleViewer viewer(NOIR, 10);
  viewer.setGeometry(10, 10, 800, 800);

  RTracer rt(&viewer);

  int t = 0;
  // GL init
  viewer.f_init = [&]() {
    Node::prim.gl_init();
  };

  viewer.f_draw = [&]() {
    Node::prim.set_matrices(viewer.getCurrentModelViewMatrix(),
                            viewer.getCurrentProjectionMatrix());
    rt.bvh = new BVH();
    float a = (float)t;
    /*
    rt.add_menger_bvh(rt.bvh, translate(0, 10, 0) * rotateX(a) * rotateY(-a) * scale(3.), ROUGE, 1.f, 1.f, 2, 1.f);
    rt.add_sphere_bvh(rt.bvh, scale(3.) * translate(0, 2, 0), JAUNE, 1.f, 0.9f, 1.33f);
    rt.add_cylinder_bvh(rt.bvh, rotateX(70) * scale(0.3, 0.3, 3) * translate(3, 13, -2.3), VERT, 0., 0.f, 1.);
    rt.add_cube_bvh(rt.bvh, scale(10, 1, 10) * translate(0, -2, 0), BLEU, 0., 0., 1.);
    rt.add_cube_bvh(rt.bvh, scale(10, 10, 1) * translate(0, 0, -5), ROUGE, 0., 0., 1.);
    rt.add_cube_bvh(rt.bvh, scale(1, 10, 10) * translate(8, 0, 0), VERT, 0., 0., 1.);
    rt.add_cube_bvh(rt.bvh, scale(1, 10, 10) * translate(-8, 0, 0), MAGENTA, 0., 0., 1.);
    */
    rt.add_menger_bvh(rt.bvh, translate(0, 3, 0) * rotateX(a) * rotateY(-a) * scale(2.), ROUGE, 1.f, .5f, 2, 1.f);
    rt.add_menger_bvh(rt.bvh, translate(0, -3, 0) * rotateX(-a) * rotateY(a) * scale(2.), BLEU, 1.f, 0.8f, 2, 1.33f);
    //rt.add_cylinder_bvh(rt.bvh, rotateX(70) * scale(0.3, 0.3, 3) * translate(3, 13, -2.3), VERT, 0., 0.5f, 1.33);
    //rt.add_cube_bvh(rt.bvh, scale(10, 1, 10) * translate(0, -2, 0), BLEU, 0., 0., 1.);
    //rt.add_cube_bvh(rt.bvh, scale(10, 10, 1) * translate(0, 0, -5), ROUGE, 0., 0., 1.);
    
    rt.bvh->build(&rt.bvh->nodes);
    draw_prim_bvh(rt.bvh);  // segfault à moins de rajouter des items au bvh
    // TC : le bvh est censé englober toute la scène.
    // Node::prim.draw_cube(Mat4(), Vec3(1., .0, .0));
  };

  viewer.f_keyPress = [&](int key, Qt::KeyboardModifiers) {
    // TC : entre 0 et 4, le nombre de rebonds pour votre rendu
    switch (key) {
      case Qt::Key_A:
        if(viewer.animationIsStarted())
          viewer.stopAnimation();
        else
          viewer.startAnimation();
        break;
      case Qt::Key_0:
        rt.CalcImage(0, "test0.png")->show();
        break;
      case Qt::Key_1:
        rt.CalcImage(1, "test1.png")->show();
        break;
      case Qt::Key_2:
        rt.CalcImage(2, "test2.png")->show();
        break;
      case Qt::Key_3:
        rt.CalcImage(3, "test3.png")->show();
        break;
      case Qt::Key_4:
        rt.CalcImage(4, "test4.png")->show();
        break;
      default:
        break;
    }
  };

  viewer.f_animate = [&]() {
    t+=1;
    char buf[32];
    sprintf(buf, "im_%d.png", t);
    printf("writing: %s\n", buf);
    rt.CalcImage(4, buf);
  };

  viewer.show();
  return a.exec();
  // TC
}
