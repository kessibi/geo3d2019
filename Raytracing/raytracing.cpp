#include "raytracing.h"
#include <emmintrin.h>
#include <pmmintrin.h>
#include <algorithm>
#include <typeinfo>

#define MEN 0.333

// Le code BBOX et BVH vient du repo Fast-BVH sur github qui a
// ete adapte aux demandes du sujet.

BBox::BBox(const Vec3& min, const Vec3& max) : min(min), max(max) {
  extent = max - min;
}

BBox::BBox(const Vec3& p) : min(p), max(p) { extent = max - min; }

void BBox::expandToInclude(const Vec3& p) {
  min = glm::min(min, p);
  max = glm::max(max, p);
  extent = max - min;
}

void BBox::expandToInclude(const BBox& b) {
  min = glm::min(min, b.min);
  max = glm::max(max, b.max);
  extent = max - min;
}

uint32_t BBox::maxDimension() const {
  uint32_t result = 0;
  if (extent.y > extent.x) {
    result = 1;
    if (extent.z > extent.y) result = 2;
  } else if (extent.z > extent.x)
    result = 2;

  return result;
}

float BBox::surfaceArea() const {
  return 2.f *
         (extent.x * extent.z + extent.x * extent.y + extent.y * extent.z);
}

// http://www.flipcode.com/archives/SSE_RayBox_Intersection_Test.shtml
// turn those verbose intrinsics into something readable.
#define loadps(mem) _mm_loadu_ps((const float* const)(mem))
#define storess(ss, mem) _mm_store_ss((float* const)(mem), (ss))
#define minss _mm_min_ss
#define maxss _mm_max_ss
#define minps _mm_min_ps
#define maxps _mm_max_ps
#define mulps _mm_mul_ps
#define divps _mm_div_ps
#define subps _mm_sub_ps
#define rotatelps(ps) _mm_shuffle_ps((ps), (ps), 0x39)  // a,b,c,d -> b,c,d,a
#define muxhps(low, high) \
  _mm_movehl_ps((low), (high))  // low{a,b,c,d}|high{e,f,g,h} = {c,d,g,h}
static const float flt_plus_inf =
    -logf(0);  // let's keep C and C++ compilers happy.
static const float __attribute__((aligned(16)))
ps_cst_plus_inf[4] = {flt_plus_inf, flt_plus_inf, flt_plus_inf, flt_plus_inf},
    ps_cst_minus_inf[4] = {-flt_plus_inf, -flt_plus_inf, -flt_plus_inf,
                           -flt_plus_inf};

bool BBox::intersect(const Vec3& o, const Vec3& inv_d, float* tnear,
                     float* tfar) const {
  // you may already have those values hanging around somewhere
  const __m128 plus_inf = loadps(ps_cst_plus_inf),
               minus_inf = loadps(ps_cst_minus_inf);

  // use whatever's apropriate to load.
  const __m128 box_min = loadps(&min), box_max = loadps(&max), pos = loadps(&o),
               inv_dir = loadps(&inv_d);

  // use a div if inverted directions aren't available
  const __m128 l1 = divps(subps(box_min, pos), inv_dir);
  const __m128 l2 = divps(subps(box_max, pos), inv_dir);

  // the order we use for those min/max is vital to filter out
  // NaNs that happens when an inv_dir is +/- inf and
  // (box_min - pos) is 0. inf * 0 = NaN
  const __m128 filtered_l1a = minps(l1, plus_inf);
  const __m128 filtered_l2a = minps(l2, plus_inf);

  const __m128 filtered_l1b = maxps(l1, minus_inf);
  const __m128 filtered_l2b = maxps(l2, minus_inf);

  // now that we're back on our feet, test those slabs.
  __m128 lmax = maxps(filtered_l1a, filtered_l2a);
  __m128 lmin = minps(filtered_l1b, filtered_l2b);

  // unfold back. try to hide the latency of the shufps & co.
  const __m128 lmax0 = rotatelps(lmax);
  const __m128 lmin0 = rotatelps(lmin);
  lmax = minss(lmax, lmax0);
  lmin = maxss(lmin, lmin0);

  const __m128 lmax1 = muxhps(lmax, lmax);
  const __m128 lmin1 = muxhps(lmin, lmin);
  lmax = minss(lmax, lmax1);
  lmin = maxss(lmin, lmin1);

  // temporary
  const bool ret =
      _mm_comige_ss(lmax, _mm_setzero_ps()) & _mm_comige_ss(lmax, lmin);
  storess(lmin, tnear);
  storess(lmax, tfar);

  return ret;
}

Primitives Node::prim;

Vec3 transformVec(Vec3 a, Mat4 b) {
  Vec3 res;
  res.x = b[0][0] * a.x + b[1][0] * a.y + b[2][0] * a.z;
  res.y = b[0][1] * a.x + b[1][1] * a.y + b[2][1] * a.z;
  res.z = b[0][2] * a.x + b[1][2] * a.y + b[2][2] * a.z;
  return res;
}

Vec3 transformPoint(Vec3 a, Mat4 b) {
  Vec3 res;
  res.x = b[0][0] * a.x + b[1][0] * a.y + b[2][0] * a.z + b[3][0];
  res.y = b[0][1] * a.x + b[1][1] * a.y + b[2][1] * a.z + b[3][1];
  res.z = b[0][2] * a.x + b[1][2] * a.y + b[2][2] * a.z + b[3][2];
  return res;
}

float clamp01(float v) {
  if (v < 0.0f) return 0.0f;
  if (v > 1.0f) return 1.0f;
  return v;
}

Vec3 Color(const Vec3& c) {
  return Vec3(clamp01(c.x), clamp01(c.y), clamp01(c.z));
}

Node::Node(const Mat4& m, const Vec3& c, float sp, float tr, float ref)
    : transfo(m),
      inv_transfo(glm::inverse(m)),
      Col(c),
      spec(sp),
      transp(tr),
      r(ref) {
  this->box = this->getBBox();
}

void Cube::draw_gl() const {
  Node::prim.draw_cube(this->transfo, this->Col);
  // DC
}
void Sphere::draw_gl() const {
  Node::prim.draw_sphere(this->transfo, this->Col);
  // DC
}
void Cylinder::draw_gl() const {
  Node::prim.draw_cylinder(this->transfo, this->Col);
  // DC
}
void Menger::draw_gl() const {
  Node::prim.draw_cube(this->transfo, this->Col);
  // DC
}

void Cube::intersecte(const Vec3& O, const Vec3& D, Inter* I) {
  Vec3 O2, D2, R;
  O2 = transformPoint(O, this->inv_transfo);
  D2 = transformVec(D, this->inv_transfo);

  float a_min, t1 = (-1 - O2.x) / D2.x;
  float t2 = (1 - O2.x) / D2.x;

  float tmin = smin(t1, t2);
  float tmax = smax(t1, t2);

  t1 = (-1 - O2.y) / D2.y;
  t2 = (1 - O2.y) / D2.y;

  tmin = smax(tmin, smin(t1, t2));
  tmax = smin(tmax, smax(t1, t2));

  t1 = (-1 - O2.z) / D2.z;
  t2 = (1 - O2.z) / D2.z;

  tmin = smax(tmin, smin(t1, t2));
  tmax = smin(tmax, smax(t1, t2));

  if (tmax > smax(tmin, 0.0)) {
    R = O + tmin * D;

    a_min = glm::distance(O, R);

    if (a_min < I->a_min) {
      I->a_min = a_min;
      I->node = this;
      I->node->center = transformPoint(Vec3(0.), this->transfo);
      I->O = R;
      I->norm = this->normal(O2 + tmin * D2);
    }
  }
}

Vec3 Cube::normal(const Vec3& V) {
  if (abs(V.x) >= 0.9999)
    return glm::normalize(transformVec(Vec3(V.x, 0., 0.), this->transfo));
  if (abs(V.y) >= 0.9999)
    return glm::normalize(transformVec(Vec3(0., V.y, 0.), this->transfo));
  if (abs(V.z) >= 0.9999)
    return glm::normalize(transformVec(Vec3(0., 0., V.z), this->transfo));
  return Vec3(0.);
}

void Sphere::intersecte(const Vec3& O, const Vec3& D, Inter* I) {
  Vec3 O2, D2, R;
  O2 = transformPoint(O, this->inv_transfo);
  D2 = transformVec(D, this->inv_transfo);

  float a = D2.x * D2.x + D2.y * D2.y + D2.z * D2.z;
  float b = 2. * (O2.x * D2.x + O2.y * D2.y + O2.z * D2.z);
  float c = (O2.x * O2.x + O2.y * O2.y + O2.z * O2.z) - 1.;

  float d = pow(b, 2) - 4. * a * c;

  if (d < 0.) return;

  float t1 = (-b - sqrt(d)) / (2. * a);
  float t2 = (-b + sqrt(d)) / (2. * a);

  d = t1 < 0. ? t2 < 0. ? -1. : t2 : t1 < t2 ? t1 : t2;
  if (d == -1.) return;
  if (d < 0.) printf("%f\n", d);

  R = O + d * D;

  float a_min = glm::distance(O, R);
  if (a_min < I->a_min) {
    I->a_min = a_min;
    I->node = this;
    I->node->center = transformPoint(Vec3(0.), this->transfo);
    I->O = R;
    I->norm = this->normal(O2 + d * D2);
  }
}

Vec3 Sphere::normal(const Vec3& V) {
  return glm::normalize(transformVec(V, glm::inverseTranspose(this->transfo)));
}

void Cylinder::intersecte(const Vec3& O, const Vec3& D, Inter* I) {
  Vec3 O2, D2, R;
  O2 = transformPoint(O, this->inv_transfo);
  D2 = transformVec(D, this->inv_transfo);

  float a_min, a = D2.x * D2.x + D2.y * D2.y;
  float b = 2. * (O2.x * D2.x + O2.y * D2.y);
  float c = O2.x * O2.x + O2.y * O2.y - 1.;

  float d = b * b - 4. * a * c;

  if (d < 0) return;

  float t1 = (-b - sqrtf(d)) / (2. * a);
  float t2 = (-b + sqrtf(d)) / (2. * a);

  if (t1 > t2) {
    float tmp = t1;
    t1 = t2;
    t2 = tmp;
  }
  float z1 = O2.z + t1 * D2.z;
  float z2 = O2.z + t2 * D2.z;

  if (z1 < -1) {
    if (z2 < -1)
      return;
    else {
      float th = t1 + (t2 - t1) * (z1 + 1) / (z1 - z2);
      if (th <= 0) return;

      R = O + th * D;
      a_min = glm::distance(O, R);
      if (a_min < I->a_min) {
        I->O = R;
        I->a_min = a_min;
        I->node = this;
        I->node->center = transformPoint(Vec3(0.), this->transfo);
        I->norm =
            glm::normalize(transformVec(Vec3(0., 0., -1.), this->transfo));
      }
    }
  } else if (z1 >= -1 && z1 <= 1) {
    if (t1 <= 0) return;

    R = O + t1 * D;
    a_min = glm::distance(O, R);
    if (a_min < I->a_min) {
      I->O = R;
      I->a_min = a_min;
      I->node = this;
      I->node->center = transformPoint(Vec3(0.), this->transfo);
      I->norm = this->normal(O2 + t1 * D2);
    }
  } else if (z1 > 1) {
    if (z2 > 1) return;

    float th = t1 + (t2 - t1) * (z1 - 1) / (z1 - z2);
    if (th <= 0) return;

    R = O + th * D;
    a_min = glm::distance(O, R);
    if (a_min < I->a_min) {
      I->O = R;
      I->a_min = a_min;
      I->node = this;
      I->node->center = transformPoint(Vec3(0.), this->transfo);
      I->norm = glm::normalize(transformVec(Vec3(0., 0., 1.), this->transfo));
    }
  }
}

Vec3 Cylinder::normal(const Vec3& V) {
  Vec3 R;
  R.x = V.x;
  R.y = V.y;
  R.z = 0.;
  return glm::normalize(transformVec(R, this->transfo));
}

struct BVHTraversal {
  uint32_t i;  // Node
  float mint;  // Minimum hit time for this node.
  BVHTraversal() {}
  BVHTraversal(int _i, float _mint) : i(_i), mint(_mint) {}
};

struct BVHBuildEntry {
  // If non-zero then this is the index of the parent. (used in offsets)
  uint32_t parent;
  // The range of objects in the object list covered by this node.
  uint32_t start, end;
};

bool BVH::getIntersection(const Vec3& P, const Vec3& V, Inter* I) const {
  I->a_min = std::numeric_limits<float>::max();
  I->node = nullptr;
  float bbhits[4];
  int32_t closer, other;

  // Working set
  BVHTraversal todo[64];
  int32_t stackptr = 0;

  // "Push" on the root node to the working set
  todo[stackptr].i = 0;
  todo[stackptr].mint = -9999999.f;

  while (stackptr >= 0) {
    // Pop off the next node to work on.
    int ni = todo[stackptr].i;
    float near = todo[stackptr].mint;
    stackptr--;
    const BVHFlatNode& node(flatTree[ni]);

    // If this node is further than the closest found intersection, continue
    if (near > I->a_min) continue;

    // Is leaf -> Intersect
    if (node.rightOffset == 0) {
      for (uint32_t o = 0; o < node.nPrims; ++o) {
        Inter current;
        current.a_min = std::numeric_limits<float>::max();

        (*build_prims)[node.start + o]->intersecte(P, V, &current);

        if (current.node) {
          // Otherwise, keep the closest intersection only
          if (current.a_min < I->a_min) {
            *I = current;
          }
        }
      }

    } else {  // Not a leaf
      bool hitc0 = flatTree[ni + 1].bbox.intersect(P, V, bbhits, bbhits + 1);
      bool hitc1 = flatTree[ni + node.rightOffset].bbox.intersect(
          P, V, bbhits + 2, bbhits + 3);

      // Did we hit both nodes?
      if (hitc0 && hitc1) {
        // We assume that the left child is a closer hit...
        closer = ni + 1;
        other = ni + node.rightOffset;

        // ... If the right child was actually closer, swap the relavent values.
        if (bbhits[2] < bbhits[0]) {
          std::swap(bbhits[0], bbhits[2]);
          std::swap(bbhits[1], bbhits[3]);
          std::swap(closer, other);
        }

        // It's possible that the nearest object is still in the other side, but
        // we'll check the further-awar node later...

        // Push the farther first
        todo[++stackptr] = BVHTraversal(other, bbhits[2]);

        // And now the closer (with overlap test)
        todo[++stackptr] = BVHTraversal(closer, bbhits[0]);
      } else if (hitc0) {
        todo[++stackptr] = BVHTraversal(ni + 1, bbhits[0]);
      } else if (hitc1) {
        todo[++stackptr] = BVHTraversal(ni + node.rightOffset, bbhits[2]);
      }
    }
  }

  return I->node != NULL;
}

BVH::~BVH() { delete[] flatTree; }

BVH::BVH(uint32_t leafSize)
    : nNodes(0), nLeafs(0), leafSize(leafSize), flatTree(NULL) {}

/*! Build the BVH, given an input data set
 *  - Handling our own stack is quite a bit faster than the recursive style.
 *  - Each build stack entry's parent field eventually stores the offset
 *    to the parent of that node. Before that is finally computed, it will
 *    equal exactly three other values. (These are the magic values Untouched,
 *    Untouched-1, and TouchedTwice).
 *  - The partition here was also slightly faster than std::partition.
 */
void BVH::build(std::vector<Node*>* objects) {
  build_prims = objects;
  BVHBuildEntry todo[128];
  uint32_t stackptr = 0;
  const uint32_t Untouched = 0xffffffff;
  const uint32_t TouchedTwice = 0xfffffffd;

  // Push the root
  todo[stackptr].start = 0;
  todo[stackptr].end = build_prims->size();
  todo[stackptr].parent = 0xfffffffc;
  stackptr++;

  BVHFlatNode node;
  std::vector<BVHFlatNode> buildnodes;
  buildnodes.reserve(build_prims->size() * 2);

  while (stackptr > 0) {
    // Pop the next item off of the stack
    BVHBuildEntry& bnode(todo[--stackptr]);
    uint32_t start = bnode.start;
    uint32_t end = bnode.end;
    uint32_t nPrims = end - start;

    nNodes++;
    node.start = start;
    node.nPrims = nPrims;
    node.rightOffset = Untouched;

    // Calculate the bounding box for this node
    BBox bb((*build_prims)[start]->getBBox());
    BBox bc((*build_prims)[start]->center);
    for (uint32_t p = start + 1; p < end; ++p) {
      bb.expandToInclude((*build_prims)[p]->getBBox());
      bc.expandToInclude((*build_prims)[p]->center);
    }
    node.bbox = bb;

    // If the number of primitives at this point is less than the leaf
    // size, then this will become a leaf. (Signified by rightOffset == 0)
    if (nPrims <= leafSize) {
      node.rightOffset = 0;
      nLeafs++;
    }

    buildnodes.push_back(node);

    // Child touches parent...
    // Special case: Don't do this for the root.
    if (bnode.parent != 0xfffffffc) {
      buildnodes[bnode.parent].rightOffset--;

      // When this is the second touch, this is the right child.
      // The right child sets up the offset for the flat tree.
      if (buildnodes[bnode.parent].rightOffset == TouchedTwice) {
        buildnodes[bnode.parent].rightOffset = nNodes - 1 - bnode.parent;
      }
    }

    // If this is a leaf, no need to subdivide.
    if (node.rightOffset == 0) continue;

    // Set the split dimensions
    uint32_t split_dim = bc.maxDimension();

    // Split on the center of the longest axis
    float split_coord = .5f * (bc.min[split_dim] + bc.max[split_dim]);

    // Partition the list of objects on this split
    uint32_t mid = start;
    for (uint32_t i = start; i < end; ++i) {
      if ((*build_prims)[i]->center[split_dim] < split_coord) {
        std::swap((*build_prims)[i], (*build_prims)[mid]);
        ++mid;
      }
    }

    // If we get a bad split, just choose the center...
    if (mid == start || mid == end) {
      mid = start + (end - start) / 2;
    }

    // Push right child
    todo[stackptr].start = mid;
    todo[stackptr].end = end;
    todo[stackptr].parent = nNodes - 1;
    stackptr++;

    // Push left child
    todo[stackptr].start = start;
    todo[stackptr].end = mid;
    todo[stackptr].parent = nNodes - 1;
    stackptr++;
  }

  // Copy the temp node data to a flat array
  flatTree = new BVHFlatNode[nNodes];
  for (uint32_t n = 0; n < nNodes; ++n) {
    flatTree[n] = buildnodes[n];
  }
}

void BVH::intersecte(const Vec3& P, const Vec3& V, Inter* I) {
  I->a_min = 99999999.;
  I->node = nullptr;
  for (unsigned long i = 0; i < nodes.size(); i++)
    nodes[i]->intersecte(P, V, I);
}

const float Node::Epsilon = 0.0001f;

RTracer::RTracer(SimpleViewer* s) : sv(s), depth(0) {}

void RTracer::add_cube_bvh(BVH* b, const Mat4& m, const Vec3& color, float spec,
                           float tr, float ref) {
  b->nodes.push_back(new Cube(m, color, spec, tr, ref));
  // DC
}
void RTracer::add_sphere_bvh(BVH* b, const Mat4& m, const Vec3& color,
                             float spec, float tr, float ref) {
  b->nodes.push_back(new Sphere(m, color, spec, tr, ref));
  // DC
}
void RTracer::add_cylinder_bvh(BVH* b, const Mat4& m, const Vec3& color,
                               float spec, float tr, float ref) {
  b->nodes.push_back(new Cylinder(m, color, spec, tr, ref));
  // DC
}
void RTracer::add_menger_bvh(BVH* b, const Mat4& m, const Vec3& color,
                             float spec, float tr, int r, float ref) {
  if (r == 0) {
    b->nodes.push_back(new Cube(m, color, spec, tr, ref));
    return;
  } else {
    Mat4 n = m;
    float f = 0.75;

    n *= scale(0.33, 0.33, 0.33);

    for (int i = -1; i < 2; i++) {
      for (int j = -1; j < 2; j++) {
        for (int k = -1; k < 2; k++) {
          if ((i != 0 && j != 0) || (j != 0 && k != 0) || (k != 0 && i != 0)) {
            Vec3 a(i * f, j * f, k * f);
            a = transformPoint(a, m);
            n[3][0] = a.x;
            n[3][1] = a.y;
            n[3][2] = a.z;
            this->add_menger_bvh(b, n, color, spec, tr, r - 1, ref);
          }
          if ((i == 0 && j == 0) || (j == 0 && k == 0) || (k == 0 && i == 0)) {
            Vec3 a(i * f, j * f, k * f);
            a = transformPoint(a, m);
            n[3][0] = a.x;
            n[3][1] = a.y;
            n[3][2] = a.z;
            b->nodes.push_back(
                new Sphere(n, Vec3(0.32, 0.4, 0.76), spec, 1.f, 1.33));
          }
        }
      }
    }
  }
}

float FresnelReflectAmount(float ior, Vec3 n, Vec3 in) {
  float cosi = glm::dot(n, in);
  if (cosi < -1.f) cosi = -1.f;
  if (cosi > 1.f) cosi = 1.f;

  float etai = 1.f, etat = ior;

  if (cosi > 0.f) std::swap(etai, etat);

  float sint = etai / etat * sqrtf(std::max(0.f, 1.f - cosi * cosi));

  if (sint >= 1.f) return 1.f;

  float cost = sqrtf(std::max(0.f, 1 - sint * sint));
  cosi = fabsf(cosi);
  float Rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
  float Rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
  return (Rs * Rs + Rp * Rp) / 2.;
}

Vec3 refracte(Vec3 i, Vec3 n, float eta) {
  eta = 2.0f - eta;
  float cosi = glm::dot(n, i);
  Vec3 o = (i * eta - n * (-cosi + eta * cosi));
  return o;
}
Vec3 RTracer::ColorRayBVH(const Vec3& P, const Vec3& V, int depth) {
  float p, t = 0.;
  Inter I, J;

  bvh->getIntersection(P, V, &I);
  if (I.node) {
    Vec3 light = glm::normalize(this->posLum - I.O);
    p = clamp01(glm::dot(light, I.norm));

    Vec3 reflect;
    Vec3 r = glm::reflect(light, I.norm);
    Inter J;
    bvh->getIntersection(I.O + 0.01f * light, this->posLum - I.O, &J);
    if (J.node && J.node != I.node)
      t = 0;
    else {
      t = glm::dot(r, glm::normalize(I.O - this->posLum));
      t = pow(clamp01(t), 7);
    }

    if (depth == 0) return Color(I.node->Col * p + t * I.node->spec * BLANC);

    if (I.node->transp < 0.01f) return Color(I.node->Col * p);

    Vec3 refColor(0.f);
    Vec3 color(0.f);
    float fres = FresnelReflectAmount(I.node->r, I.norm, V);

    if (fres < 1) {
      Vec3 refract = refracte(V, I.norm, I.node->r);
      refColor = ColorRayBVH(I.O + 0.01f * refract, refract, depth - 1);
    }

    reflect = glm::reflect(V, I.norm);
    Vec3 reflectColor = ColorRayBVH(I.O + 0.01f * reflect, reflect, depth - 1);

    color += reflectColor * fres + refColor * (1 - fres);
    color = color * I.node->transp + (1.f - I.node->transp) * I.node->Col * p;

    return Color(color + I.node->spec * t * BLANC);
  } else
    return NOIR;
}

void RTracer::calc_thr(QImage* im, int t) {
  for (int y = t; y < this->sv->height(); y += this->nbthr) {
    for (int x = 0; x < this->sv->width(); ++x) {
      qglviewer::Vec Pq = this->sv->camera()->unprojectedCoordinatesOf(
          qglviewer::Vec(x, y, 0.0));
      qglviewer::Vec Qq = this->sv->camera()->unprojectedCoordinatesOf(
          qglviewer::Vec(x, y, 1.0));
      Vec3 P(Pq[0], Pq[1], Pq[2]);
      Vec3 D(Qq[0] - Pq[0], Qq[1] - Pq[1], Qq[2] - Pq[2]);
      Vec3 C = this->ColorRayBVH(P, D, this->depth);
      // ^ ou ColorRay(...) : lance un rayon d'origine P et de direction D,
      // rebondissant this->depth fois.
      im->setPixel(
          x, y,
          QColor(int(C.r * 255.0f), int(C.g * 255.0f), int(C.b * 255.0f))
              .rgb());
    }
  }
  // DC
}

QLabel* RTracer::CalcImage(int depth, const char* filename) {
  QImage im(this->sv->width(), this->sv->height(), QImage::Format_RGB32);
  Vec4 SHADER_POSLUM(30, 100, 100, 1);
  this->posLum =
      Vec3(glm::inverse(this->sv->getCurrentModelViewMatrix()) * SHADER_POSLUM);
  this->depth = depth;

  std::vector<std::thread*> threads;
  threads.reserve(this->nbthr);

  for (int i = 0; i < this->nbthr; ++i) {
    // En C++ on utilise les threads comme ceci.
    // La fonction lambda passée en paramètre du constructeur est exécutée par
    // un thread.
    threads.push_back(
        new std::thread([i, &im, this]() { this->calc_thr(&im, i); }));
  }

  for (auto* t : threads) {
    // join fonctionne comme une barrière, où le thread principal attend que
    // chaque thread ait terminé pour le libérer. il est également possible
    // d'utiliser des mutex avec std::mutex, mais c'est une autre histoire.
    t->join();
    delete t;
  }
  im.save(filename, 0, 100);
  QLabel* label_img = new QLabel(this->sv);
  label_img->setWindowFlags(Qt::Window);
  label_img->setPixmap(QPixmap::fromImage(im));
  return label_img;
  // DC
}

void draw_prim_bvh(BVH* b) {
  for (const auto* n : b->nodes) n->draw_gl();
}
