#ifndef __RAYTRACING_H__
#define __RAYTRACING_H__

#define smin(a, b) ((a) < (b) ? (a) : (b))
#define smax(a, b) ((a) > (b) ? (a) : (b))

#include <pmmintrin.h>
#include <stdint.h>
#include <xmmintrin.h>
#include <thread>
#include "QGLViewer/simple_viewer.h"
#include "matrices.h"
#include "primitives.h"

#include <QApplication>
#include <QLabel>

const Vec3 ROUGE = {1, 0, 0};       // 0
const Vec3 VERT = {0, 1, 0};        // 1
const Vec3 BLEU = {0, 0, 1};        // 2
const Vec3 JAUNE = {1, 1, 0};       // 3
const Vec3 CYAN = {0, 1, 1};        // 4
const Vec3 MAGENTA = {1, 0, 1};     // 5
const Vec3 BLANC = {1, 1, 1};       // 6
const Vec3 GRIS = {0.5, 0.5, 0.5};  // 7
const Vec3 NOIR = {0, 0, 0};        // 8
const Vec3 ROUGE2 = {0.5, 0, 0};    // 9
const Vec3 VERT2 = {0, 0.5, 0};     // 10
const Vec3 ORANGE = {1, 0.5, 0};    // 11

// transformer un point selon la matrice de transfo donne
Vec3 transformPoint(Vec3 a, Mat4 b);
// transformer un vecteur selon la matrice de transfo donne
Vec3 transformVec(Vec3 a, Mat4 b);
// indice de reflection rendu par fresnel
float FresnelReflectAmount(float ior, Vec3 n, Vec3 in);
inline Vec3 getMin(Vec3 v[]) {
  Vec3 res(9999999.);
  for (int i = 0; i < 8; i++) {
    res.x = smin(res.x, v[i].x);
    res.y = smin(res.y, v[i].y);
    res.z = smin(res.z, v[i].z);
  }
  return res;
}
inline Vec3 getMax(Vec3 v[]) {
  Vec3 res(-9999999.);
  for (int i = 0; i < 8; i++) {
    res.x = smax(res.x, v[i].x);
    res.y = smax(res.y, v[i].y);
    res.z = smax(res.z, v[i].z);
  }
  return res;
}

struct BBox {
  Vec3 min, max, extent;
  Vec3 cmin, cmax, cextent;
  BBox() {}
  BBox(const Vec3& min, const Vec3& max);
  BBox(const Vec3& p);

  bool intersect(const Vec3& o, const Vec3& inv_d, float* tnear,
                 float* tfar) const;
  void expandToInclude(const Vec3& p);
  void expandToInclude(const BBox& b);
  uint32_t maxDimension() const;
  float surfaceArea() const;
};

class Node;
struct Inter {
  float a_min;    // distance de l'intersection.
  int inter_dir;  // direction de l'intersection.
  Node* node;     // noeud intercepté, if any.
  Vec3 O;         // point d'impact du rayon.
  Vec3 D;  // direction du rayon. (ps : ctrl+shift+R sur qtcreator pour changer
           // le nom des variables).
  Vec3 norm;  // normale a l'objet rencontre a l'intersection
  Vec3 reflect();
  Vec3 refract();
};

class Node {
 public:
  static const float Epsilon;
  Mat4 transfo;  // matrice de transformation.
  Mat4 inv_transfo;
  Vec3 Col;      // couleur.
  float spec;    // indice de spécularité.
  float transp;  // indice de transparence.
  float r;       // indice de refraction.
  Vec3 center;   // center of the node
  BBox box;

  Node(const Mat4& m, const Vec3& c, float sp, float tr, float ref);
  inline virtual ~Node() {}
  virtual void draw_gl() const = 0;  // la fonction permettant de dessiner la
                                     // primitive dans la vue temps réel.
  virtual void intersecte(const Vec3& P, const Vec3& V,
                          Inter* I) = 0;  // test d'intersection de la primitive
                                          // (modifier contenu de I).
  virtual Vec3 normal(const Vec3& V) = 0;  // la normale à la primitive
  static Primitives prim;

  inline BBox getBBox() const {
    Vec3 a(-1.), b(1.), v[8];
    a = transformPoint(a, transfo);
    b = transformPoint(b, transfo);
    v[0] = transformPoint(Vec3(1., 1., 1.), transfo);
    v[1] = transformPoint(Vec3(1., -1., 1.), transfo);
    v[2] = transformPoint(Vec3(1., 1., -1.), transfo);
    v[3] = transformPoint(Vec3(-1., 1., 1.), transfo);
    v[4] = transformPoint(Vec3(1., -1., -1.), transfo);
    v[5] = transformPoint(Vec3(-1., 1., -1.), transfo);
    v[6] = transformPoint(Vec3(-1., -1., 1.), transfo);
    v[7] = transformPoint(Vec3(1., 1., 1.), transfo);

    Vec3 min, max;
    min = getMin(v);
    max = getMax(v);

    return BBox(min, max);
  }
};

class Cube : public Node {
 public:
  inline Cube(const Mat4& m, const Vec3& c, float sp, float tr, float ref)
      : Node(m, c, sp, tr, ref) {
    center.x = m[3][0];
    center.y = m[3][1];
    center.z = m[3][2];
  }
  void draw_gl() const;
  void intersecte(const Vec3& P, const Vec3& V, Inter* I);
  Vec3 normal(const Vec3& V);
};

class Menger : public Node {
 public:
  int rec = 0;
  std::vector<Menger*> c;
  inline Menger(const Mat4& m, const Vec3& c, float sp, float tr, float r,
                float ref)
      : Node(m, c, sp, tr, ref) {
    rec = r;
    center.x = m[3][0];
    center.y = m[3][1];
    center.z = m[3][2];
  }
  void draw_gl() const;
};

class Sphere : public Node {
 public:
  inline Sphere(const Mat4& m, const Vec3& c, float sp, float tr, float ref)
      : Node(m, c, sp, tr, ref) {
    center.x = m[3][0];
    center.y = m[3][1];
    center.z = m[3][2];
  }
  void draw_gl() const;
  void intersecte(const Vec3& P, const Vec3& V, Inter* I);
  Vec3 normal(const Vec3& V);
};

class Cylinder : public Node {
 public:
  inline Cylinder(const Mat4& m, const Vec3& c, float sp, float tr, float ref)
      : Node(m, c, sp, tr, ref) {
    center.x = m[3][0];
    center.y = m[3][1];
    center.z = m[3][2];
  }
  void draw_gl() const;
  void intersecte(const Vec3& P, const Vec3& V, Inter* I);
  Vec3 normal(const Vec3& V);
};

struct BVHFlatNode {
  BBox bbox;
  uint32_t start, nPrims, rightOffset;
};

class BVH {
 public:
  uint32_t nNodes, nLeafs, leafSize;
  std::vector<Node*>* build_prims;
  std::vector<Node*> nodes;

  //! Build the BVH tree out of build_prims
  void build(std::vector<Node*>* objects);
  inline void add_node(Node* n) { nodes.push_back(n); }

  // Fast Traversal System
  BVHFlatNode* flatTree;

  void intersecte(const Vec3& P, const Vec3& V, Inter* I);

  BVH(uint32_t leafSize = 4);
  bool getIntersection(const Vec3& P, const Vec3& V, Inter* I) const;

  ~BVH();
};

class RTracer {
 public:
  BVH* bvh;
  SimpleViewer* sv;
  std::vector<Node*> primitives;
  int depth;
  Vec3 posLum;
  static const int nbthr = 4;

  RTracer(SimpleViewer* s);

  void add_cube_bvh(BVH* b, const Mat4& m, const Vec3& color, float spec,
                    float tr, float ref);
  void add_sphere_bvh(BVH* b, const Mat4& m, const Vec3& color, float spec,
                      float tr, float ref);
  void add_menger_bvh(BVH* b, const Mat4& m, const Vec3& color, float spec,
                      float tr, int r, float ref);
  void add_cylinder_bvh(BVH* b, const Mat4& m, const Vec3& color, float spec,
                        float tr, float ref);

  QLabel* CalcImage(int depth, const char* filename);
  // calcule l'image par lancer de rayons.
  // Renvoie de quoi l'afficher.
  void calc_thr(QImage* im, int t);  // calcul d'un seul thread.

  // v   à utiliser si vous voulez tester un rendu de la scène avant
  // d'implémenter un bvh.
  Vec3 ColorRay(const Vec3& P, const Vec3& V, int depth);
  Vec3 ColorRayBVH(const Vec3& P, const Vec3& V, int depth);
};

void draw_prim_bvh(BVH* b);

#endif
